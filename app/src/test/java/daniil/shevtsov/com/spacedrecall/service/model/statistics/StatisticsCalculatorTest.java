package daniil.shevtsov.com.spacedrecall.service.model.statistics;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.ReviewLog;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.SessionLog;
import daniil.shevtsov.com.spacedrecall.service.repository.database.DeckProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.ReviewLogsRepository;
import daniil.shevtsov.com.spacedrecall.service.repository.database.SessionLogsRepository;
import daniil.shevtsov.com.spacedrecall.util.TestDataProvider;
import io.reactivex.Single;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;

public class StatisticsCalculatorTest {
    private static final List<SessionLog> SESSION_LOGS = TestDataProvider.getTestSessionLogs();
    private static final List<ReviewLog> REVIEW_LOGS = TestDataProvider.getTestReviewLogs();
    private static final List<Deck> DECKS = TestDataProvider.getTestDecks();

    private static final double AVERAGE_ARRIVAL_RATE = 3.5;

    private static final int NEW_CARDS_COUNT = 2;

    private static final double AVERAGE_COUNT = NEW_CARDS_COUNT;

    private static final double AVERAGE_REVIEW_FREQUENCY_BUDGET = 3;

    private static final List<Double> OPTIMAL_SERVICE_RATES = Arrays.asList(0.5, 0.2);
    private static final List<Long> OPTIMAL_REPETITION_INTERVALS = Arrays.asList(2L, 5L);

    @Mock
    private ReviewLogsRepository mReviewLogsRepository;

    @Mock
    private SessionLogsRepository mSessionLogsRepository;

    @Mock
    private DeckProvider mDeckProvider;

    private StatisticsCalculator mStatisticsCalculator;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        doReturn(Single.just(SESSION_LOGS)).when(mSessionLogsRepository).getSessionLogs();

        doReturn(Single.just(REVIEW_LOGS)).when(mReviewLogsRepository).getReviewLogs();
        doReturn(Single.just(DECKS)).when(mDeckProvider).getAllDecks();
        doReturn(Single.just(NEW_CARDS_COUNT)).when(mReviewLogsRepository).countNewCardsInDeck(anyLong(), anyLong());

        mStatisticsCalculator = new StatisticsCalculator(mReviewLogsRepository, mSessionLogsRepository, mDeckProvider);
    }

    @Test
    public void testCalculateArrivalRate() {
        mStatisticsCalculator.calculateOptimizationInputData()
                .test()
                .assertValue(optimizationInputData -> optimizationInputData.getArrivalRate() == AVERAGE_ARRIVAL_RATE);
    }

    @Test
    public void testCalculateInjectionRates() {
        mStatisticsCalculator.calculateOptimizationInputData()
                .test()
                .assertValue(optimizationInputData -> optimizationInputData.getInjectionRates().size() == DECKS.size())
                .assertValue(optimizationInputData -> optimizationInputData.getInjectionRates().get(0).equals(AVERAGE_COUNT));
    }

    @Test
    public void testCalculateReviewFrequencyBudget() {
        mStatisticsCalculator.calculateOptimizationInputData()
                .test()
                .assertValue(optimizationInputData -> optimizationInputData.getReviewFrequencyBudget() == AVERAGE_REVIEW_FREQUENCY_BUDGET);
    }

    @Test
    public void testCalculateRepetitionIntervals() {
        List<Long> repetitionIntervals = mStatisticsCalculator.calculateRepetitionIntervals(OPTIMAL_SERVICE_RATES);

        assertThat(repetitionIntervals, hasItems(OPTIMAL_REPETITION_INTERVALS.get(0),
                OPTIMAL_REPETITION_INTERVALS.get(1)));
    }
}
