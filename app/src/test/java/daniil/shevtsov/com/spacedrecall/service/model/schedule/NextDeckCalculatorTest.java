package daniil.shevtsov.com.spacedrecall.service.model.schedule;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class NextDeckCalculatorTest {
    private static final int PREVIOUS_DECK_NUMBER = 1;
    private static final int CURRENT_DECK_NUMBER = 2;
    private static final int NEXT_DECK_NUMBER = 3;

    private static final int MIN_DECK_NUMBER = NextDeckCalculator.MIN_DECK_NUMBER;
    private static final int MAX_DECK_NUMBER = NextDeckCalculator.MAX_DECK_NUMBER;


    private NextDeckCalculator mNextDeckCalculator;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        mNextDeckCalculator = new NextDeckCalculator();
    }

    @Test
    public void testNextDeckNumberCalculation() {
        int newDeckNumber = mNextDeckCalculator.calculateDeckNumber(CURRENT_DECK_NUMBER, true);

        assertThat(newDeckNumber, is(NEXT_DECK_NUMBER));
    }

    @Test
    public void testPreviousDeckNumberCalculation() {
        int newDeckNumber = mNextDeckCalculator.calculateDeckNumber(PREVIOUS_DECK_NUMBER, false);

        assertThat(newDeckNumber, is(PREVIOUS_DECK_NUMBER));
    }

    @Test
    public void testMinDeckNumberCalculation() {
        int newDeckNumber = mNextDeckCalculator.calculateDeckNumber(MIN_DECK_NUMBER, false);

        assertThat(newDeckNumber, is(MIN_DECK_NUMBER));
    }

    @Test
    public void testMaxDeckNumberCalculation() {
        int newDeckNumber = mNextDeckCalculator.calculateDeckNumber(MAX_DECK_NUMBER, true);

        assertThat(newDeckNumber, is(MAX_DECK_NUMBER));
    }
}
