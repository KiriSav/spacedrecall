package daniil.shevtsov.com.spacedrecall.service.repository.database;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.room.model.dao.DeckDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import daniil.shevtsov.com.spacedrecall.util.TestDataProvider;
import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class DeckProviderTest {
    private static final List<Deck> DECKS = TestDataProvider.getTestDecks();
    private static final Deck FIRST_DECK = TestDataProvider.getTestFirstDeck();

    private static final int NUMBER_OF_DECKS = 5;

    private static final List<Long> REPETITION_INTERVALS = Arrays.asList(1L, 2L, 3L);

    private static final int NEXT_DECK_NUMBER = 1;

    @Mock
    DeckDao mDeckDao;

    @Mock
    Logger mLogger;

    @Captor
    ArgumentCaptor<List<Deck>> mInsertDecksArguments;

    @Captor
    ArgumentCaptor<Long> mRepetitionIntervalArgument;

    private Scheduler mTestScheduler;

    private DeckProvider mDeckProvider;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        mTestScheduler = Schedulers.trampoline();

        doReturn(Single.just(DECKS)).when(mDeckDao).selectAllDecks();
        doReturn(Single.just(FIRST_DECK)).when(mDeckDao).selectFirstDeck();
        doReturn(Single.just(FIRST_DECK)).when(mDeckDao).selectDeckWithId(FIRST_DECK.getDeckId());
        doReturn(Single.just(FIRST_DECK)).when(mDeckDao).selectDeckWithNumber(NEXT_DECK_NUMBER);

        doReturn(Completable.complete()).when(mDeckDao).updateRepetitionInterval(anyLong(), anyLong());

        mDeckProvider = new DeckProvider(mDeckDao, mLogger);
    }

    @Test
    public void testCreateDecks() {
        doReturn(Single.just(new ArrayList<>())).when(mDeckDao).insert(anyList());

        mDeckProvider.createDecks(NUMBER_OF_DECKS);

        verify(mDeckDao).insert(mInsertDecksArguments.capture());
        List<Deck> createdDecks = mInsertDecksArguments.getValue();
        assertThat(createdDecks.size(), is(NUMBER_OF_DECKS));
    }

    @Test
    public void testGetEarliestDeck() {
        mDeckProvider.getEarliestDeck()
                .subscribeOn(mTestScheduler)
                .test()
                .assertValue(deck -> deck.getNumber().equals(FIRST_DECK.getNumber()));
    }

    @Test
    public void testGetFirstDeckId() {
        mDeckProvider.getFirstDeckId()
                .test()
                .assertValue(FIRST_DECK.getDeckId());
    }

    @Test
    public void testGetDeckForId() {
        mDeckProvider.getDeckForId(FIRST_DECK.getDeckId())
                .observeOn(mTestScheduler)
                .test()
                .assertValue(deck -> deck.getNumber().equals(FIRST_DECK.getNumber()));
    }

    @Test
    public void testCheckIfDeckExists() {
        doReturn(Single.just(1)).when(mDeckDao).selectDecksCount();

        mDeckProvider.checkIfDecksExist()
                .observeOn(mTestScheduler)
                .test()
                .assertValue(true);
    }

    @Test
    public void testCheckIfDeckExistsEmpty() {
        doReturn(Single.just(0)).when(mDeckDao).selectDecksCount();

        mDeckProvider.checkIfDecksExist()
                .observeOn(mTestScheduler)
                .test()
                .assertValue(false);
    }

    @Test
    public void testUpdateRepetitionIntervals() {
        mDeckProvider.updateRepetitionIntervals(REPETITION_INTERVALS)
                .subscribeOn(mTestScheduler)
                .blockingAwait();

        verify(mDeckDao, times(REPETITION_INTERVALS.size())).updateRepetitionInterval(anyLong(), mRepetitionIntervalArgument.capture());
        assertThat(mRepetitionIntervalArgument.getAllValues(), is(REPETITION_INTERVALS));
    }

    @Test
    public void testGetDeckForNumber() {
        mDeckProvider.getDeckForNumber(NEXT_DECK_NUMBER)
                .subscribeOn(mTestScheduler)
                .test()
                .assertValue(deck -> deck.getNumber().equals(FIRST_DECK.getNumber()));
    }
}
