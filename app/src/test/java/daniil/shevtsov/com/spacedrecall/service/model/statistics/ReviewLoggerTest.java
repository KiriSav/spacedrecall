package daniil.shevtsov.com.spacedrecall.service.model.statistics;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.ReviewLog;
import daniil.shevtsov.com.spacedrecall.service.repository.CurrentReviewLogsStorage;
import daniil.shevtsov.com.spacedrecall.service.repository.TimestampProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.ReviewLogsRepository;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import daniil.shevtsov.com.spacedrecall.util.TestDataProvider;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class ReviewLoggerTest {
    private static final long NEW_DECK_ID = 1L;
    private static final long CURRENT_TIMESTAMP = 10L;
    private static final long CURRENT_SESSION_ID = 4L;

    private static final List<ReviewLog> REVIEW_LOGS = TestDataProvider.getTestReviewLogs();

    @Mock
    private TimestampProvider mTimestampProvider;

    @Mock
    private ReviewLogsRepository mReviewLogsRepository;

    @Mock
    private CurrentReviewLogsStorage mCurrentReviewLogsStorage;

    @Mock
    private Logger mLogger;

    @Captor
    private ArgumentCaptor<ReviewLog> mOnCardReviewedCaptor;

    private ReviewLogger mReviewLogger;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        mReviewLogger = new ReviewLogger(mTimestampProvider, mReviewLogsRepository,
                mCurrentReviewLogsStorage, mLogger);
    }

    @Test
    public void testOnCardReviewed() {
        doReturn(CURRENT_TIMESTAMP).when(mTimestampProvider).getCurrentTimestamp();

        mReviewLogger.onCardReviewed(NEW_DECK_ID);

        verify(mCurrentReviewLogsStorage).addReviewLog(mOnCardReviewedCaptor.capture());

        ReviewLog capturedReviewLog = mOnCardReviewedCaptor.getValue();
        assertThat(capturedReviewLog.getNewDeckId(), is(NEW_DECK_ID));
        assertThat(capturedReviewLog.getTimestamp(), is(CURRENT_TIMESTAMP));
    }

    @Test
    public void onSessionFinished() {
        doReturn(REVIEW_LOGS).when(mCurrentReviewLogsStorage).getReviewLogs();

        mReviewLogger.onSessionFinished(CURRENT_SESSION_ID);

        verify(mCurrentReviewLogsStorage).updateSessionId(CURRENT_SESSION_ID);

        verify(mReviewLogsRepository).insertReviewLogs(REVIEW_LOGS);
    }

}
