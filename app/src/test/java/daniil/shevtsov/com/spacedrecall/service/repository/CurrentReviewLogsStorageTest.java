package daniil.shevtsov.com.spacedrecall.service.repository;

import org.junit.Before;
import org.junit.Test;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.ReviewLog;
import daniil.shevtsov.com.spacedrecall.util.TestDataProvider;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItems;

public class CurrentReviewLogsStorageTest {
    private static final long SESSION_ID = 5L;

    private static final ReviewLog REVIEW_LOG = TestDataProvider.getTestFirstReviewLog();

    private CurrentReviewLogsStorage mCurrentReviewLogsStorage;

    @Before
    public void onSetup() {
        mCurrentReviewLogsStorage = new CurrentReviewLogsStorage();
    }

    @Test
    public void testAddNewReviewLog() {
        mCurrentReviewLogsStorage.addReviewLog(REVIEW_LOG);

        assertThat(mCurrentReviewLogsStorage.getReviewLogs(), hasItems(REVIEW_LOG));
    }

    @Test
    public void testUpdateSessionId() {
        mCurrentReviewLogsStorage.addReviewLog(REVIEW_LOG);

        mCurrentReviewLogsStorage.updateSessionId(SESSION_ID);

        assertThat(mCurrentReviewLogsStorage.getReviewLogs().get(0).getSessionLogId(), is(SESSION_ID));
    }
}
