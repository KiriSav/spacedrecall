package daniil.shevtsov.com.spacedrecall.service.model.statistics;

import javax.inject.Inject;
import javax.inject.Named;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.SessionLog;
import daniil.shevtsov.com.spacedrecall.dagger.module.SchedulerModule;
import daniil.shevtsov.com.spacedrecall.service.repository.CurrentSessionLogStorage;
import daniil.shevtsov.com.spacedrecall.service.repository.TimestampProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.SessionLogsRepository;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import io.reactivex.Scheduler;

public class SessionLogger {
    private static final String TAG = SessionLogger.class.getSimpleName();

    private final Logger mLogger;
    private final ReviewLogger mReviewLogger;
    private final TimestampProvider mTimestampProvider;
    private final SessionLogsRepository mSessionLogsRepository;
    private final CurrentSessionLogStorage mCurrentSessionLogStorage;

    private final Scheduler mBackgroundScheduler;
    private final Scheduler mMainScheduler;


    @Inject
    public SessionLogger(Logger logger, ReviewLogger reviewLogger, TimestampProvider timestampProvider,
                         SessionLogsRepository sessionLogsRepository, CurrentSessionLogStorage currentSessionLogStorage,
                         @Named(SchedulerModule.BACKGROUND_THREAD) Scheduler backgroundScheduler,
                         @Named(SchedulerModule.MAIN_THREAD) Scheduler mainScheduler) {
        mLogger = logger;
        mReviewLogger = reviewLogger;
        mTimestampProvider = timestampProvider;
        mSessionLogsRepository = sessionLogsRepository;
        mCurrentSessionLogStorage = currentSessionLogStorage;

        mBackgroundScheduler = backgroundScheduler;
        mMainScheduler = mainScheduler;
    }

    public void onSessionStarted() {
        mLogger.d(TAG, "onSessionStarted");
        mCurrentSessionLogStorage.setSessionStartedTimestamp(mTimestampProvider.getCurrentTimestamp());
    }

    public void onSessionFinished() {
        mLogger.d(TAG, "onSessionFinished");
        mCurrentSessionLogStorage.setSessionFinishedTimestamp(mTimestampProvider.getCurrentTimestamp());

        mCurrentSessionLogStorage.setReviewFrequencyBudget();

        SessionLog sessionLog = mCurrentSessionLogStorage.getCurrentSessionLog();

        mSessionLogsRepository.addNewSessionLog(sessionLog)
                .flatMapCompletable(mReviewLogger::onSessionFinished)
        .subscribeOn(mBackgroundScheduler)
        .observeOn(mMainScheduler)
        .subscribe(() -> mLogger.d(TAG, "inserted session log"));
    }

    public void onCardAdded() {
        mCurrentSessionLogStorage.increaseAddedCardsCount();
    }

    public void onCardReviewed() {
        mLogger.d(TAG, "onCardReviewed");
        mCurrentSessionLogStorage.increaseReviewedCardsCount();
    }

    public void onCardMemorized() {
        mCurrentSessionLogStorage.increaseMemorizedCardsCount();
    }
}
