package daniil.shevtsov.com.spacedrecall.service.model.statistics;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.SessionLog;
import daniil.shevtsov.com.spacedrecall.service.model.schedule.OptimizationInputData;
import daniil.shevtsov.com.spacedrecall.service.repository.database.DeckProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.ReviewLogsRepository;
import daniil.shevtsov.com.spacedrecall.service.repository.database.SessionLogsRepository;
import io.reactivex.Single;

public class StatisticsCalculator {
    private final ReviewLogsRepository mReviewLogsRepository;
    private final SessionLogsRepository mSessionLogsRepository;
    private final DeckProvider mDeckProvider;

    @Inject
    public StatisticsCalculator(ReviewLogsRepository reviewLogsRepository,
                                SessionLogsRepository sessionLogsRepository, DeckProvider deckProvider) {
        mReviewLogsRepository = reviewLogsRepository;

        mSessionLogsRepository = sessionLogsRepository;
        mDeckProvider = deckProvider;
    }

    public Single<OptimizationInputData> calculateOptimizationInputData() {
        return calculateAverageArrivalRate()
                .flatMap(averageArrivalRate -> calculateAverageInjectionRates()
                        .flatMap(injectionRates -> calculateFrequencyBudget()
                                .map(reviewFrequencyBudget ->
                                        new OptimizationInputData(averageArrivalRate, injectionRates, reviewFrequencyBudget))
                        ));
    }

    private Single<Double> calculateAverageArrivalRate() {
        return mSessionLogsRepository.getSessionLogs()
                .flatMap(sessionLogs -> {
                    double arrivalRatesSum = 0;

                    for (SessionLog sessionLog : sessionLogs) {
                        long sessionTime = sessionLog.getSessionEndTimestamp() - sessionLog.getSessionStartTimestamp();
                        double sessionArrivalRate = (double) sessionLog.getAddedCardsCount() / sessionTime;
                        arrivalRatesSum += sessionArrivalRate;
                    }

                    double averageArrivalRate = arrivalRatesSum / sessionLogs.size();

                    return Single.just(averageArrivalRate);
                });
    }

    private Single<List<Double>> calculateAverageInjectionRates() {
        return mDeckProvider.getAllDecks()
                .flattenAsObservable(decks -> decks)
                .flatMap(deck -> calculateAverageInjectionForDeck(deck.getDeckId()).toObservable())
                .toList();
    }

    private Single<Double> calculateFrequencyBudget() {
        return mSessionLogsRepository.getSessionLogs()
                .map(sessionLogs -> {
                    double averageFrequencyBudgetSum = 0;

                    for (SessionLog sessionLog : sessionLogs) {
                        averageFrequencyBudgetSum += sessionLog.getReviewFrequencyBudget();
                    }

                    double averageFrequencyBudget = averageFrequencyBudgetSum / sessionLogs.size();

                    return averageFrequencyBudget;
                });
    }

    public List<Long> calculateRepetitionIntervals(List<Double> optimalServiceRates) {
        List<Long> repetitionIntervals = new ArrayList<>();

        for (Double serviceRate : optimalServiceRates) {
            long repetitionInterval = (long) (1 / serviceRate);
            repetitionIntervals.add(repetitionInterval);
        }

        return repetitionIntervals;
    }

    private Single<Double> calculateAverageInjectionForDeck(Long deckId) {
        return mSessionLogsRepository.getSessionLogs()
                .flattenAsObservable(sessionLogs -> sessionLogs)
                .flatMap(sessionLog -> mReviewLogsRepository.countNewCardsInDeck(deckId, sessionLog.getSessionLogId())
                        .toObservable())
                .toList()
                .map(sessionCounts -> {
                    double deckAverageInjectionRate = 0;
                    for (Integer sessionCount : sessionCounts) {
                        deckAverageInjectionRate += sessionCount;
                    }

                    deckAverageInjectionRate = deckAverageInjectionRate / sessionCounts.size();

                    return deckAverageInjectionRate;
                });
    }
}
