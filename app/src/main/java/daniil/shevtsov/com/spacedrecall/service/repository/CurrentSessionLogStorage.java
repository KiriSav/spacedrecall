package daniil.shevtsov.com.spacedrecall.service.repository;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.SessionLog;

public class CurrentSessionLogStorage {
    private SessionLog mCurrentSessionLog;

    @Inject
    public CurrentSessionLogStorage() {
        mCurrentSessionLog = new SessionLog(0L,0L,0,0,0,0.0);
    }

    public void setSessionStartedTimestamp(long timestamp) {
        mCurrentSessionLog.setSessionStartTimestamp(timestamp);
    }

    public void setSessionFinishedTimestamp(long timestamp) {
        mCurrentSessionLog.setSessionEndTimestamp(timestamp);
    }

    public void increaseAddedCardsCount() {
        int currentCount = mCurrentSessionLog.getAddedCardsCount();
        mCurrentSessionLog.setAddedCardsCount(++currentCount);
    }

    public void increaseReviewedCardsCount() {
        int currentCount = mCurrentSessionLog.getReviewedCardsCount();
        mCurrentSessionLog.setReviewedCardsCount(++currentCount);
    }

    public void increaseMemorizedCardsCount() {
        int currentCount = mCurrentSessionLog.getMemorizedCardsCount();
        mCurrentSessionLog.setMemorizedCardsCount(++currentCount);
    }

    public void setReviewFrequencyBudget() {
        //TODO: encapsulate this logic somewhere
        int cardsReviewedCount = mCurrentSessionLog.getReviewedCardsCount();
        long sessionStartTimestamp = mCurrentSessionLog.getSessionStartTimestamp();
        long sessionEndTimestamp = mCurrentSessionLog.getSessionEndTimestamp();

        double reviewFrequencyBudget = (double) cardsReviewedCount / (sessionEndTimestamp - sessionStartTimestamp);


        mCurrentSessionLog.setReviewFrequencyBudget(reviewFrequencyBudget);
    }

    public SessionLog getCurrentSessionLog() {
        return mCurrentSessionLog;
    }

    public long getSessionStartTimestamp() {
        return mCurrentSessionLog.getSessionStartTimestamp();
    }

    public long getSessionFinishTimestamp() {
        return mCurrentSessionLog.getSessionEndTimestamp();
    }

    public int getAddedCardsCount() {
        return mCurrentSessionLog.getAddedCardsCount();
    }

    public int getReviewedCardsCount() {
        return mCurrentSessionLog.getReviewedCardsCount();
    }

    public int getMemorizedCardsCount() {
        return mCurrentSessionLog.getMemorizedCardsCount();
    }

    public double getReviewFrequencyBudget() {
        return mCurrentSessionLog.getReviewFrequencyBudget();
    }
}
