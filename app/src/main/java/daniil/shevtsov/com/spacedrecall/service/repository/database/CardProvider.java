package daniil.shevtsov.com.spacedrecall.service.repository.database;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.room.model.dao.CardDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import io.reactivex.Completable;
import io.reactivex.Single;

public class CardProvider {
    private final CardDao mCardDao;

    @Inject
    public CardProvider(CardDao cardDao) {
        mCardDao = cardDao;
    }

    public Single<Card> getEarliestCardFromDeck(long deckId) {
        return mCardDao.selectEarliestCardFromDeck(deckId);
    }

    public Completable moveCardToAnotherDeck(Card card, long newDeckId) {
        return mCardDao.updateCardDeckId(card.getCardId(), newDeckId);
    }

    public Completable updateLastRepetitionTime(Long cardId, long newLastRepetitionTime) {
        return mCardDao.updateCardLastRepetitionTimestamp(cardId, newLastRepetitionTime);
    }

    public Completable addNewCard(Card newCard) {
        return mCardDao.insert(newCard)
                .toCompletable();
    }

    public Single<Integer> countCardsAvailableForRepetition(Long deckId) {
        return mCardDao.countCardsAvailableForRepetition(deckId);
    }
}
