package daniil.shevtsov.com.spacedrecall.service.model.schedule;

import java.util.List;

public class OptimizationInputData {
    private final double mArrivalDate;
    private final List<Double> mInjectionRates;
    private final double mReviewFrequencyBudget;


    public OptimizationInputData(double arrivalDate, List<Double> injectionRates,
                                 double reviewFrequencyBudget) {
        mArrivalDate = arrivalDate;
        mInjectionRates = injectionRates;
        mReviewFrequencyBudget = reviewFrequencyBudget;
    }

    public double getArrivalRate() {
        return mArrivalDate;
    }

    public List<Double> getInjectionRates() {
        return mInjectionRates;
    }

    public double getReviewFrequencyBudget() {
        return mReviewFrequencyBudget;
    }
}
