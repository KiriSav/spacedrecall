package daniil.shevtsov.com.spacedrecall.service.model.datetime;

import java.util.Calendar;
import java.util.Date;

public class Clock {
    public Calendar getCalendarInstance() {
        return Calendar.getInstance();
    }

    Date newDate() {
        return new Date();
    }
}
