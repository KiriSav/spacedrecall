package daniil.shevtsov.com.spacedrecall.service.repository;

import java.util.Date;

import javax.inject.Inject;

public class TimestampProvider {

    @Inject
    TimestampProvider() {
    }

    public long getCurrentTimestamp() {
       Date date = new Date();
       return date.getTime();
    }

}
