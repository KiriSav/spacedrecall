package daniil.shevtsov.com.spacedrecall.api.room.model.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "review_logs_table",
        foreignKeys = {
                @ForeignKey(entity = SessionLog.class,
                        parentColumns = "session_log_id",
                        childColumns = "session_log_id",
                        onDelete = ForeignKey.CASCADE),
                @ForeignKey(entity = Deck.class,
                        parentColumns = "deck_id",
                        childColumns = "new_deck_id",
                        onDelete = ForeignKey.CASCADE)
        }
)
public class ReviewLog {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "review_log_id")
    @NonNull
    private Long mReviewLogId;

    @ColumnInfo(name = "session_log_id")
    private Long mSessionLogId;

    @ColumnInfo(name = "new_deck_id")
    private Long mNewDeckId;

    @ColumnInfo(name = "timestamp")
    private final Long mTimestamp;

    @Ignore
    public ReviewLog(@NonNull Long reviewLogId, Long sessionLogId, Long newDeckId, Long timestamp) {
        mReviewLogId = reviewLogId;
        mSessionLogId = sessionLogId;
        mNewDeckId = newDeckId;
        mTimestamp = timestamp;
    }

    public ReviewLog(Long sessionLogId, Long newDeckId, Long timestamp) {
        this(null, sessionLogId, newDeckId, timestamp);
    }

    @NonNull
    public Long getReviewLogId() {
        return mReviewLogId;
    }

    public void setReviewLogId(@NonNull Long reviewLogId) {
        mReviewLogId = reviewLogId;
    }

    public Long getSessionLogId() {
        return mSessionLogId;
    }

    public Long getNewDeckId() {
        return mNewDeckId;
    }

    public void setNewDeckId(Long newDeckId) {
        mNewDeckId = newDeckId;
    }

    public Long getTimestamp() {
        return mTimestamp;
    }

    public void setSessionLogId(Long sessionLogId) {
        mSessionLogId = sessionLogId;
    }
}
