package daniil.shevtsov.com.spacedrecall.service.model.datetime;

import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

public class DateWorker {
    private final Clock mClock;

    @Inject
    public DateWorker(Clock clock) {
        mClock = clock;
    }

    public Date getCurrentDate() {
        Calendar calendar = mClock.getCalendarInstance();
        return calendar.getTime();
    }

    public Date addDays(int days) {
        Calendar calendar = mClock.getCalendarInstance();
        calendar.add(Calendar.DATE, days);

        return calendar.getTime();
    }

}
