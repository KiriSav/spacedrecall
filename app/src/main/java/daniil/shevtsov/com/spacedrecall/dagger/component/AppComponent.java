package daniil.shevtsov.com.spacedrecall.dagger.component;

import daniil.shevtsov.com.spacedrecall.android.MyApplication;
import daniil.shevtsov.com.spacedrecall.view.ui.AddCardsActivity;
import daniil.shevtsov.com.spacedrecall.view.ui.MainActivity;
import daniil.shevtsov.com.spacedrecall.view.ui.StudyCardsActivity;


public interface AppComponent {
    void inject(MainActivity mainActivity);

    void inject(AddCardsActivity addCardsActivity);

    void inject(StudyCardsActivity studyCardsActivity);

    void inject(MyApplication myApplication);
}
