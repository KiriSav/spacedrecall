package daniil.shevtsov.com.spacedrecall.api.room;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import daniil.shevtsov.com.spacedrecall.api.room.model.dao.CardDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.dao.DeckDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.dao.ReviewLogDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.dao.SessionLogDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.ReviewLog;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.SessionLog;
import daniil.shevtsov.com.spacedrecall.util.DateTypeConverter;

@Database(entities = {Card.class, Deck.class, SessionLog.class, ReviewLog.class}, version = 11)
@TypeConverters({DateTypeConverter.class})
public abstract class CardsDatabase extends RoomDatabase {
    public static final String DATABASE_NAME = "cards_database";

    public static final String SESSION_LOGS_TABLE_NAME = "session_logs_table";

    public abstract CardDao cardDao();

    public abstract DeckDao deckDao();

    public abstract SessionLogDao sessionLogDao();

    public abstract ReviewLogDao reviewLogDao();

    /*
     * Migrate from:
     * version 3 - column is called word*
     * to
     * version 4 - column is called question
     */
    private static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE cards_table_new (" +
                    "id INTEGER NOT NULL, " +
                    "question TEXT NOT NULL, " +
                    "translation TEXT NOT NULL, " +
                    "next_repetition_date INTEGER NOT NULL, " +
                    "PRIMARY KEY(id))");

            database.execSQL(
                    "INSERT INTO cards_table_new (id, question, translation, next_repetition_date)" +
                            "SELECT id, word, translation, next_repetition_date " +
                            "FROM cards_table");

            database.execSQL("DROP TABLE cards_table");

            database.execSQL("ALTER TABLE cards_table_new RENAME TO cards_table");
        }
    };

    /*
     * Migrate from:
     * version 4 - column is called translation
     * to
     * version 5 - column is called answer
     */
    private static final Migration MIGRATION_4_5 = new Migration(4, 5) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE cards_table_new (" +
                    "id INTEGER NOT NULL, " +
                    "question TEXT NOT NULL, " +
                    "answer TEXT NOT NULL, " +
                    "next_repetition_date INTEGER NOT NULL, " +
                    "PRIMARY KEY(id))");

            database.execSQL(
                    "INSERT INTO cards_table_new (id, question, answer, next_repetition_date)" +
                            "SELECT id, question, translation, next_repetition_date " +
                            "FROM cards_table");

            database.execSQL("DROP TABLE cards_table");

            database.execSQL("ALTER TABLE cards_table_new RENAME TO cards_table");
        }
    };

    /*
     * Migrate from:
     * version 3 - columns are word and translation
     * to
     * version 5 - columns are question and answer
     */
    private static final Migration MIGRATION_3_5 = new Migration(3, 5) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE cards_table_new (" +
                    "id INTEGER NOT NULL, " +
                    "question TEXT NOT NULL, " +
                    "answer TEXT NOT NULL, " +
                    "next_repetition_date INTEGER NOT NULL, " +
                    "PRIMARY KEY(id))");

            database.execSQL(
                    "INSERT INTO cards_table_new (id, question, answer, next_repetition_date)" +
                            "SELECT id, word, translation, next_repetition_date " +
                            "FROM cards_table");

            database.execSQL("DROP TABLE cards_table");

            database.execSQL("ALTER TABLE cards_table_new RENAME TO cards_table");
        }
    };

    /*
     * Migrate from:
     * version 5 - primary key is called id, has next_repetition_date column
     * to
     * version 6 - primary key is called card_id, does not have next_repetition_date column,
     *             but there are last_repetition_date and deck_id columns.
     *             create decks_table
     */
    private static final Migration MIGRATION_5_6 = new Migration(5, 6) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE cards_table_new (" +
                    "card_id INTEGER NOT NULL, " +
                    "question TEXT, " +
                    "answer TEXT, " +
                    "last_repetition_date INTEGER, " +
                    "deck_id INTEGER , " +
                    "PRIMARY KEY(card_id))");

            database.execSQL("CREATE TABLE decks_table (" +
                    "deck_id INTEGER NOT NULL," +
                    "number INTEGER," +
                    "repetition_interval INTEGER," +
                    "PRIMARY KEY(deck_id))");

            database.execSQL(
                    "INSERT INTO cards_table_new (card_id, question, answer)" +
                            "SELECT id, question, answer " +
                            "FROM cards_table");

            database.execSQL("DROP TABLE cards_table");

            database.execSQL("ALTER TABLE cards_table_new RENAME TO cards_table");
        }
    };

    /*
     * Migrate from:
     * version 6 - deck_id in cards_table is just a column
     * to
     * version 7 - deck_id in cards_table is a foreign key
     */
    private static final Migration MIGRATION_6_7 = new Migration(6, 7) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE cards_table_new (" +
                    "card_id INTEGER NOT NULL, " +
                    "question TEXT, " +
                    "answer TEXT, " +
                    "last_repetition_date INTEGER, " +
                    "deck_id INTEGER , " +
                    "PRIMARY KEY(card_id)," +
                    "FOREIGN KEY(deck_id) REFERENCES decks_table(deck_id))");

            database.execSQL(
                    "INSERT INTO cards_table_new (card_id, question, answer, last_repetition_date, deck_id)" +
                            "SELECT card_id, question, answer, last_repetition_date, deck_id " +
                            "FROM cards_table");

            database.execSQL("DROP TABLE cards_table");

            database.execSQL("ALTER TABLE cards_table_new RENAME TO cards_table");
        }
    };

    /*
     * Migrate from:
     * version 7 - no session log and review log tables
     * to
     * version 8 - session logs and review logs tables created
     */
    private static final Migration MIGRATION_7_8 = new Migration(7, 8) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE session_logs_table (" +
                    "session_log_id INTEGER NOT NULL, " +
                    "session_start_timestamp INTEGER, " +
                    "session_end_timestamp INTEGER, " +
                    "added_cards_count INTEGER, " +
                    "reviewed_cards_count INTEGER , " +
                    "memorized_cards_count INTEGER," +
                    "review_frequency_budget REAL," +
                    "PRIMARY KEY(session_log_id))");

            database.execSQL("CREATE TABLE review_logs_table (" +
                    "review_log_id INTEGER NOT NULL, " +
                    "session_log_id INTEGER, " +
                    "new_deck_id INTEGER, " +
                    "timestamp INTEGER, " +
                    "PRIMARY KEY(review_log_id)," +
                    "FOREIGN KEY(session_log_id) REFERENCES session_logs_table(session_log_id)," +
                    "FOREIGN KEY(new_deck_id) REFERENCES decks_table(deck_id))");
        }
    };

    /*
     * Migrate from:
     * version 8 - no last_repetition_timestamp column in decks_table
     * to
     * version 9 - created last_repetition_timestamp column in decks_table
     */
    private static final Migration MIGRATION_8_9 = new Migration(8, 9) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
//
            database.execSQL("CREATE TABLE decks_table_new (" +
                    "deck_id INTEGER NOT NULL, " +
                    "number INTEGER, " +
                    "repetition_interval INTEGER, " +
                    "last_repetition_timestamp INTEGER, " +
                    "PRIMARY KEY(deck_id))");

            database.execSQL(
                    "INSERT INTO decks_table_new (deck_id, number, repetition_interval, last_repetition_timestamp) " +
                            "SELECT deck_id, number, repetition_interval, NULL " +
                            "FROM decks_table");

            database.execSQL("DROP TABLE decks_table");

            database.execSQL("ALTER TABLE decks_table_new RENAME TO decks_table");
        }
    };

    /*
     * Migrate from:
     * version 9 - no next_repetition_timestamp column in decks_table,
     * no first_added_timestamp in cards_table
     * to
     * version 10 - created next_repetition_timestamp column in decks_table
     * and first_added_timestamp in cards_table
     */
    private static final Migration MIGRATION_9_10 = new Migration(9, 10) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE decks_table_new (" +
                    "deck_id INTEGER NOT NULL, " +
                    "number INTEGER, " +
                    "repetition_interval INTEGER, " +
                    "last_repetition_timestamp INTEGER, " +
                    "next_repetition_timestamp INTEGER, " +
                    "PRIMARY KEY(deck_id))");

            database.execSQL(
                    "INSERT INTO decks_table_new (deck_id, number, repetition_interval, " +
                            "last_repetition_timestamp, next_repetition_timestamp) " +
                            "SELECT deck_id, number, repetition_interval, last_repetition_timestamp, NULL " +
                            "FROM decks_table");

            database.execSQL("DROP TABLE decks_table");

            database.execSQL("ALTER TABLE decks_table_new RENAME TO decks_table");

            database.execSQL("CREATE TABLE cards_table_new (" +
                    "card_id INTEGER NOT NULL, " +
                    "question TEXT, " +
                    "answer TEXT, " +
                    "last_repetition_date INTEGER, " +
                    "first_added_timestamp INTEGER, " +
                    "deck_id INTEGER , " +
                    "PRIMARY KEY(card_id)," +
                    "FOREIGN KEY(deck_id) REFERENCES decks_table(deck_id))");

            database.execSQL(
                    "INSERT INTO cards_table_new (card_id, question, answer, last_repetition_date, " +
                            "first_added_timestamp, deck_id)" +
                            "SELECT card_id, question, answer, last_repetition_date, NULL, deck_id " +
                            "FROM cards_table");

            database.execSQL("DROP TABLE cards_table");

            database.execSQL("ALTER TABLE cards_table_new RENAME TO cards_table");
        }
    };

    /*
     * Migrate from:
     * version 10 - review logs and cards are not delete on cascade
     * to
     * version 11 - review logs and cards are delete on cascade
     */
    private static final Migration MIGRATION_10_11 = new Migration(10, 11) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE cards_table_new (" +
                    "card_id INTEGER NOT NULL, " +
                    "question TEXT, " +
                    "answer TEXT, " +
                    "last_repetition_date INTEGER, " +
                    "first_added_timestamp INTEGER, " +
                    "deck_id INTEGER , " +
                    "PRIMARY KEY(card_id)," +
                    "FOREIGN KEY(deck_id) " +
                    "REFERENCES decks_table(deck_id)" +
                    "ON DELETE CASCADE)");

            database.execSQL(
                    "INSERT INTO cards_table_new (card_id, question, answer, last_repetition_date, " +
                            "first_added_timestamp, deck_id)" +
                            "SELECT card_id, question, answer, last_repetition_date, first_added_timestamp, deck_id " +
                            "FROM cards_table");

            database.execSQL("DROP TABLE cards_table");

            database.execSQL("ALTER TABLE cards_table_new RENAME TO cards_table");

            database.execSQL("CREATE TABLE review_logs_table_new (" +
                    "review_log_id INTEGER NOT NULL, " +
                    "session_log_id INTEGER, " +
                    "new_deck_id INTEGER, " +
                    "timestamp INTEGER, " +
                    "PRIMARY KEY(review_log_id)," +
                    "FOREIGN KEY(session_log_id) " +
                    "REFERENCES session_logs_table(session_log_id)" +
                    "ON DELETE CASCADE," +
                    "FOREIGN KEY(new_deck_id) " +
                    "REFERENCES decks_table(deck_id)" +
                    "ON DELETE CASCADE)");

            database.execSQL("INSERT INTO review_logs_table_new (review_log_id, session_log_id, new_deck_id, timestamp) " +
                    "SELECT review_log_id, session_log_id, new_deck_id, timestamp FROM review_logs_table");

            database.execSQL("DROP TABLE review_logs_table");

            database.execSQL("ALTER TABLE review_logs_table_new RENAME TO review_logs_table");
        }
    };

    public static final Migration[] MIGRATIONS = {MIGRATION_3_4, MIGRATION_4_5, MIGRATION_3_5, MIGRATION_5_6,
            MIGRATION_6_7, MIGRATION_7_8, MIGRATION_8_9, MIGRATION_9_10, MIGRATION_10_11};

}
