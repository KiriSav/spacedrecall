package daniil.shevtsov.com.spacedrecall.service.repository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.ReviewLog;

public class CurrentReviewLogsStorage {
    private List<ReviewLog> mReviewLogs;

    @Inject
    public CurrentReviewLogsStorage() {
        mReviewLogs = new ArrayList<>();
    }

    public void addReviewLog(ReviewLog reviewLog) {
        mReviewLogs.add(reviewLog);
    }

    public void updateSessionId(long sessionId) {
        for(ReviewLog reviewLog : mReviewLogs) {
            reviewLog.setSessionLogId(sessionId);
        }
    }

    public List<ReviewLog> getReviewLogs() {
        return mReviewLogs;
    }
}
