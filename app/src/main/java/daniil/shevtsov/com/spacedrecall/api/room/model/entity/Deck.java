package daniil.shevtsov.com.spacedrecall.api.room.model.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "decks_table")
public class Deck {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "deck_id")
    @NonNull
    private Long mDeckId;

    @ColumnInfo(name = "number")
    private Integer mNumber;

    @ColumnInfo(name = "repetition_interval")
    private final Long mRepetitionInterval;

    @ColumnInfo(name = "last_repetition_timestamp")
    private final Long mLastRepetitionTimestamp;

    @ColumnInfo(name = "next_repetition_timestamp")
    private Long mNextRepetitionTimestamp;

    @Ignore
    public Deck(@NonNull Long deckId, Integer number, Long repetitionInterval,
                Long lastRepetitionTimestamp, Long nextRepetitionTimestamp) {
        mDeckId = deckId;
        mNumber = number;
        mRepetitionInterval = repetitionInterval;
        mLastRepetitionTimestamp = lastRepetitionTimestamp;
        mNextRepetitionTimestamp = nextRepetitionTimestamp;
    }

    public Deck(Integer number, Long repetitionInterval, Long lastRepetitionTimestamp, Long nextRepetitionTimestamp) {
        this(null, number, repetitionInterval, lastRepetitionTimestamp, nextRepetitionTimestamp);
    }

    @NonNull
    public Long getDeckId() {
        return mDeckId;
    }

    public void setDeckId(@NonNull Long deckId) {
        mDeckId = deckId;
    }

    public Integer getNumber() {
        return mNumber;
    }

    public void setNumber(Integer number) {
        mNumber = number;
    }

    public Long getRepetitionInterval() {
        return mRepetitionInterval;
    }

    public Long getLastRepetitionTimestamp() {
        return mLastRepetitionTimestamp;
    }

    public Long getNextRepetitionTimestamp() {
        return mNextRepetitionTimestamp;
    }

    public void setNextRepetitionTimestamp(Long nextRepetitionTimestamp) {
        mNextRepetitionTimestamp = nextRepetitionTimestamp;
    }
}
