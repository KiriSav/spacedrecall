package daniil.shevtsov.com.spacedrecall.dagger.module;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;
import daniil.shevtsov.com.spacedrecall.service.model.statistics.ReviewLogger;
import daniil.shevtsov.com.spacedrecall.service.model.statistics.SessionLogger;
import daniil.shevtsov.com.spacedrecall.service.repository.CurrentReviewLogsStorage;
import daniil.shevtsov.com.spacedrecall.service.repository.CurrentSessionLogStorage;
import daniil.shevtsov.com.spacedrecall.service.repository.TimestampProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.ReviewLogsRepository;
import daniil.shevtsov.com.spacedrecall.service.repository.database.SessionLogsRepository;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import io.reactivex.Scheduler;

@Module
public class StatisticsModule {
    @Provides
    @ApplicationScope
    CurrentSessionLogStorage provideCurrentSessionLogStorage() {
        return new CurrentSessionLogStorage();
    }

    @Provides
    @ApplicationScope
    ReviewLogger provideReviewLogger(TimestampProvider timestampProvider, ReviewLogsRepository reviewLogsRepository,
                                     CurrentReviewLogsStorage currentReviewLogsStorage, Logger logger) {
        return new ReviewLogger(timestampProvider, reviewLogsRepository, currentReviewLogsStorage, logger);
    }

    @Provides
    @ApplicationScope
    SessionLogger provideSessionLogger(Logger logger, ReviewLogger reviewLogger, TimestampProvider timestampProvider,
                                       SessionLogsRepository sessionLogsRepository,
                                       CurrentSessionLogStorage currentSessionLogStorage,
                                       @Named(SchedulerModule.BACKGROUND_THREAD) Scheduler backgroundScheduler,
                                       @Named(SchedulerModule.MAIN_THREAD) Scheduler mainScheduler) {
        return new SessionLogger(logger, reviewLogger, timestampProvider, sessionLogsRepository, currentSessionLogStorage, backgroundScheduler, mainScheduler);
    }
}
