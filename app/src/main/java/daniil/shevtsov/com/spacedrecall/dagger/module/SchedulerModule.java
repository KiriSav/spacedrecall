package daniil.shevtsov.com.spacedrecall.dagger.module;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Module
public class SchedulerModule {
    public static final String BACKGROUND_THREAD = "background";
    public static final String MAIN_THREAD = "main";

    @Provides
    @ApplicationScope
    @Named(BACKGROUND_THREAD)
    Scheduler provideBackgroundScheduler() {
        return Schedulers.io();
    }

    @Provides
    @ApplicationScope
    @Named(MAIN_THREAD)
    Scheduler provideMainScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
