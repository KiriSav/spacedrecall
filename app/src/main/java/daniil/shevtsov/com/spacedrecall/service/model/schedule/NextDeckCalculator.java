package daniil.shevtsov.com.spacedrecall.service.model.schedule;

import androidx.annotation.VisibleForTesting;

import javax.inject.Inject;

public class NextDeckCalculator {
    @VisibleForTesting
    public static final int MIN_DECK_NUMBER = 1;

    public static final int MAX_DECK_NUMBER = 5;

    @Inject
    public NextDeckCalculator() {

    }

    public int calculateDeckNumber(int currentDeckNumber, boolean isCardRecalled) {
        return (isCardRecalled) ? calculateNextDeckNumber(currentDeckNumber)
                : calculatePreviousDeckNumber(currentDeckNumber);
    }

    private int calculateNextDeckNumber(int currentDeckNumber) {
        if(currentDeckNumber < MAX_DECK_NUMBER) {
            return ++currentDeckNumber;
        } else {
            return MAX_DECK_NUMBER;
        }

    }

    private int calculatePreviousDeckNumber(int currentDeckNumber) {
        if(currentDeckNumber > MIN_DECK_NUMBER) {
            return --currentDeckNumber;
        } else {
            return MIN_DECK_NUMBER;
        }
    }
}
