package daniil.shevtsov.com.spacedrecall.viewmodel;

import androidx.lifecycle.ViewModel;

import javax.inject.Inject;
import javax.inject.Named;

import daniil.shevtsov.com.spacedrecall.dagger.module.SchedulerModule;
import daniil.shevtsov.com.spacedrecall.service.model.schedule.NextDeckCalculator;
import daniil.shevtsov.com.spacedrecall.service.model.statistics.SessionLogger;
import daniil.shevtsov.com.spacedrecall.service.repository.database.DeckProvider;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import daniil.shevtsov.com.spacedrecall.util.SingleLiveEvent;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;

public class MainViewModel extends ViewModel {
    private static final String TAG = MainViewModel.class.getSimpleName();

    //TODO: find where to store max deck number instead of in NextDeckCalculator
    private static final int NUMBER_OF_DECKS = NextDeckCalculator.MAX_DECK_NUMBER;

    private final Logger mLogger;
    private final SessionLogger mSessionLogger;
    private final DeckProvider mDeckProvider;

    private final Scheduler mBackgroundScheduler;
    private final Scheduler mMainScheduler;

    private final CompositeDisposable mCompositeDisposable;

    private final SingleLiveEvent<Void> mNavigateToAddCardsEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Void> mNavigateToStudyEvent = new SingleLiveEvent<>();

    @Inject
    public MainViewModel(Logger logger, SessionLogger sessionLogger, DeckProvider deckProvider,
                         @Named(SchedulerModule.BACKGROUND_THREAD) Scheduler backgroundScheduler,
                         @Named(SchedulerModule.MAIN_THREAD) Scheduler mainScheduler) {
        mLogger = logger;
        mSessionLogger = sessionLogger;
        mDeckProvider = deckProvider;
        mBackgroundScheduler = backgroundScheduler;
        mMainScheduler = mainScheduler;

        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        super.onCleared();

        mCompositeDisposable.dispose();
    }

    public SingleLiveEvent<Void> getNavigateToAddCardsEvent() {
        return mNavigateToAddCardsEvent;
    }

    public SingleLiveEvent<Void> getNavigateToStudyEvent() {
        return mNavigateToStudyEvent;
    }

    public void initialiseDecks() {
        mLogger.d(TAG, "onSessionStarted");

        //TODO: probably should not be done here
        mDeckProvider.checkIfDecksExist()
                .subscribeOn(mBackgroundScheduler)
                .filter(isDeckExists -> !isDeckExists)
                .flatMapCompletable(isDeckExists -> mDeckProvider.createDecks(NUMBER_OF_DECKS))
        .blockingAwait();
    }

    public void onAddCards() {
        mNavigateToAddCardsEvent.call();
    }

    public void onStudyCards() {
        mNavigateToStudyEvent.call();
    }
}
