package daniil.shevtsov.com.spacedrecall.api.room.model.dao;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.SessionLog;
import daniil.shevtsov.com.spacedrecall.util.TestDatabaseUtil;
import daniil.shevtsov.com.spacedrecall.util.dagger.TestApplication;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

@RunWith(AndroidJUnit4.class)
public class SessionLogDaoTest {
    private static final long AWAIT_TIMEOUT = TestDatabaseUtil.getAwaitTimeout();

    private static final SessionLog FIRST_SESSION_LOG = TestDatabaseUtil.getFirstSessionLog();
    private static final SessionLog SECOND_SESSION_LOG = TestDatabaseUtil.getSecondSessionLog();

    private static final List<SessionLog> SESSION_LOGS = TestDatabaseUtil.getSessionLogs();
    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Inject
    TestDatabaseUtil mTestDatabaseUtil;

    private Scheduler mTestScheduler;

    private SessionLogDao mSessionLogDao;

    @Before
    public void onSetup() {
        mTestScheduler = Schedulers.io();

        TestApplication testApplication = (TestApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();

        testApplication.buildTestAppComponent();

        testApplication.getAppComponent().inject(this);

        mSessionLogDao = mTestDatabaseUtil.getCardsDatabase().sessionLogDao();

        mTestDatabaseUtil.populateTestDatabase();
    }

    @After
    public void onTearDown() {
        mTestDatabaseUtil.tearDownTestDatabase();
    }

    @Test
    public void testSelectSessionLogWithId() {
        mSessionLogDao.selectSessionLogWithId(FIRST_SESSION_LOG.getSessionLogId())
                .subscribeOn(mTestScheduler)
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(sessionLog ->
                        sessionLog.getAddedCardsCount().equals(FIRST_SESSION_LOG.getAddedCardsCount()));

    }

    @Test
    public void testInsertSessionLogList() {
        mSessionLogDao.insert(SESSION_LOGS)
                .flatMap(sessionLogId -> mSessionLogDao.selectAllSessionLogs())
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(sessionLogs -> sessionLogs.get(0).getAddedCardsCount().equals(SESSION_LOGS.get(0).getAddedCardsCount())
                        && sessionLogs.get(1).getAddedCardsCount().equals(SESSION_LOGS.get(1).getAddedCardsCount()));

    }

    @Test
    public void testSelectAllSessionLogs() {
        mSessionLogDao.selectAllSessionLogs()
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(sessionLogs -> sessionLogs.get(0).getAddedCardsCount().equals(FIRST_SESSION_LOG.getAddedCardsCount())
                                && sessionLogs.get(1).getAddedCardsCount().equals(SECOND_SESSION_LOG.getAddedCardsCount()));
    }
}
