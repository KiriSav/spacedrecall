package daniil.shevtsov.com.spacedrecall.util.dagger;

import android.content.Context;

import androidx.room.Room;

import dagger.Module;
import dagger.Provides;
import daniil.shevtsov.com.spacedrecall.api.room.CardsDatabase;
import daniil.shevtsov.com.spacedrecall.dagger.module.CommonRoomModule;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;

@Module(includes = CommonRoomModule.class)
public class TestRoomModule {
    @Provides
    @ApplicationScope
    CardsDatabase provideInMemoryCardsDatabase(Context context) {
        return Room.inMemoryDatabaseBuilder(context, CardsDatabase.class)
                .addMigrations(CardsDatabase.MIGRATIONS)
                .build();
    }
}
