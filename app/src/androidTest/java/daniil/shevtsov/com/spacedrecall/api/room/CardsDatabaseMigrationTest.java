package daniil.shevtsov.com.spacedrecall.api.room;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import androidx.room.Room;
import androidx.room.migration.Migration;
import androidx.room.testing.MigrationTestHelper;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory;
import androidx.test.InstrumentationRegistry;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;
import java.sql.Date;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class CardsDatabaseMigrationTest {
    private static final String TEST_DATABASE_NAME = "test_migration_database";

    private static final String CARDS_TABLE_NAME = "cards_table";
    private static final String REVIEW_TABLE_NAME = "review_logs_table";
    private static final String DECKS_TABLE_NAME = "decks_table";

    private static final long TEST_CARD_ID = 1;
    private static final String TEST_CARD_QUESTION = "lol";
    private static final String TEST_CARD_ANSWER = "kek";
    private static final long TEST_CARD_NEXT_REPETITION_DATE = new Date(1L).getTime();
    private static final long TEST_CARD_LAST_REPETITION_DATE = new Date(2L).getTime();
    private static final long FIRST_ADDED_TIMESTAMP = 5L;

    private static final long TEST_DECK_ID = 1;
    private static final int TEST_DECK_NUMBER = 1;
    private static final long TEST_REPETITION_INTERVAL = 60;
    private static final long LAST_REPETITION_TIMESTAMP = 2L;

    private static final long TEST_REVIEW_LOG_ID = 1;
    private static final long TEST_SESSION_LOG_ID = 2;
    private static final long TEST_NEW_DECK_ID = 3;
    private static final long TEST_TIMESTAMP = 4;

    private static final int CURRENT_FINAL_MIGRATION = 11;
    private static final Migration[] MIGRATIONS = CardsDatabase.MIGRATIONS;

    private Scheduler mTestScheduler;

    @Rule
    public final MigrationTestHelper mMigrationTestHelper = new MigrationTestHelper(
            InstrumentationRegistry.getInstrumentation(),
            CardsDatabase.class.getCanonicalName(),
            new FrameworkSQLiteOpenHelperFactory()
    );

    @Before
    public void onSetup() {
        mTestScheduler = Schedulers.trampoline();
    }

    private CardsDatabase getMigratedDatabase(int initialDatabaseVersion,
                                              TableInsertion... tableInsertions) throws IOException {
        SupportSQLiteDatabase originalDatabase = mMigrationTestHelper.createDatabase(TEST_DATABASE_NAME, initialDatabaseVersion);

        for (TableInsertion tableInsertion : tableInsertions) {
            originalDatabase.insert(tableInsertion.getTableName(), SQLiteDatabase.CONFLICT_REPLACE, tableInsertion.mContentValues);
        }

        originalDatabase.close();

        mMigrationTestHelper.runMigrationsAndValidate(TEST_DATABASE_NAME, CURRENT_FINAL_MIGRATION,
                true, MIGRATIONS);

        return Room.databaseBuilder(InstrumentationRegistry.getTargetContext(),
                CardsDatabase.class, TEST_DATABASE_NAME)
                .addMigrations(MIGRATIONS)
                .build();
    }

    @Test
    public void testMigrationFrom5ContainsCorrectData() throws IOException {
        ContentValues cardContentValues = new ContentValues();
        cardContentValues.put("id", TEST_CARD_ID);
        cardContentValues.put("question", TEST_CARD_QUESTION);
        cardContentValues.put("answer", TEST_CARD_ANSWER);
        cardContentValues.put("next_repetition_date", TEST_CARD_NEXT_REPETITION_DATE);

        TableInsertion cardTableInsertion = new TableInsertion(CARDS_TABLE_NAME, cardContentValues);

        CardsDatabase migratedDatabase = getMigratedDatabase(5, cardTableInsertion);

        migratedDatabase.cardDao().selectOneCard()
                .subscribeOn(mTestScheduler)
                .test()
                .assertValue(card -> card.getQuestion().equals(TEST_CARD_QUESTION)
                        && card.getAnswer().equals(TEST_CARD_ANSWER)
                        && (card.getDeckId() == null)
                        && (card.getLastRepetitionDate() == null));
    }

    @Test
    public void testMigrationFrom6ContainsCorrectData() throws IOException {
        ContentValues cardValues = new ContentValues();
        cardValues.put("card_id", TEST_CARD_ID);
        cardValues.put("question", TEST_CARD_QUESTION);
        cardValues.put("answer", TEST_CARD_ANSWER);
        cardValues.put("last_repetition_date", TEST_CARD_LAST_REPETITION_DATE);
        cardValues.put("deck_id", TEST_DECK_ID);
        TableInsertion cardTableInsertion = new TableInsertion(CARDS_TABLE_NAME, cardValues);

        ContentValues deckValues = new ContentValues();
        deckValues.put("deck_id", TEST_DECK_ID);
        deckValues.put("number", TEST_DECK_NUMBER);
        deckValues.put("repetition_interval", TEST_REPETITION_INTERVAL);
        TableInsertion deckTableInsertion = new TableInsertion(DECKS_TABLE_NAME, deckValues);

        CardsDatabase migratedDatabase = getMigratedDatabase(6, cardTableInsertion, deckTableInsertion);

        migratedDatabase.cardDao().selectOneCard()
                .subscribeOn(mTestScheduler)
                .test()
                .assertValue(card -> card.getQuestion().equals(TEST_CARD_QUESTION)
                        && card.getAnswer().equals(TEST_CARD_ANSWER)
                        && (card.getDeckId() == TEST_DECK_ID)
                        && (card.getLastRepetitionDate() == TEST_CARD_LAST_REPETITION_DATE));

        migratedDatabase.deckDao().selectOneDeck()
                .subscribeOn(mTestScheduler)
                .test()
                .assertValue(deck -> deck.getDeckId() == TEST_DECK_ID
                        && deck.getNumber() == TEST_DECK_NUMBER
                        && deck.getRepetitionInterval() == TEST_REPETITION_INTERVAL);
    }

    @Test
    public void testMigrationFrom8To9ContainsCorrectData() throws IOException {
        ContentValues deckValues = new ContentValues();
        deckValues.put("deck_id", TEST_DECK_ID);
        deckValues.put("number", TEST_DECK_NUMBER);
        deckValues.put("repetition_interval", TEST_REPETITION_INTERVAL);
        TableInsertion deckInsertion = new TableInsertion(DECKS_TABLE_NAME, deckValues);

        CardsDatabase migratedDatabase = getMigratedDatabase(8, deckInsertion);

        migratedDatabase.deckDao().selectOneDeck()
                .subscribeOn(mTestScheduler)
                .test()
                .assertValue(deck -> deck.getDeckId() == TEST_DECK_ID
                        && deck.getNumber() == TEST_DECK_NUMBER
                        && deck.getRepetitionInterval() == TEST_REPETITION_INTERVAL
                        && deck.getLastRepetitionTimestamp() == null);
    }

    @Test
    public void testMigrationFrom9To10ContainsCorrectData() throws IOException {
        ContentValues deckValues = new ContentValues();
        deckValues.put("deck_id", TEST_DECK_ID);
        deckValues.put("number", TEST_DECK_NUMBER);
        deckValues.put("repetition_interval", TEST_REPETITION_INTERVAL);
        deckValues.put("last_repetition_timestamp", LAST_REPETITION_TIMESTAMP);
        TableInsertion deckTableInsertion = new TableInsertion(DECKS_TABLE_NAME, deckValues);

        ContentValues cardValues = new ContentValues();
        cardValues.put("card_id", TEST_CARD_ID);
        cardValues.put("question", TEST_CARD_QUESTION);
        cardValues.put("answer", TEST_CARD_ANSWER);
        cardValues.put("last_repetition_date", TEST_CARD_LAST_REPETITION_DATE);
        cardValues.put("deck_id", TEST_DECK_ID);
        TableInsertion cardTableInsertion = new TableInsertion(CARDS_TABLE_NAME, cardValues);

        CardsDatabase migratedDatabase = getMigratedDatabase(9, deckTableInsertion, cardTableInsertion);

        migratedDatabase.deckDao().selectOneDeck()
                .subscribeOn(mTestScheduler)
                .test()
                .assertValue(deck -> deck.getDeckId() == TEST_DECK_ID
                        && deck.getNumber() == TEST_DECK_NUMBER
                        && deck.getRepetitionInterval() == TEST_REPETITION_INTERVAL
                        && deck.getLastRepetitionTimestamp() == LAST_REPETITION_TIMESTAMP
                        && deck.getNextRepetitionTimestamp() == null);

        migratedDatabase.cardDao().selectOneCard()
                .subscribeOn(mTestScheduler)
                .test()
                .assertValue(card -> card.getQuestion().equals(TEST_CARD_QUESTION)
                        && card.getAnswer().equals(TEST_CARD_ANSWER)
                        && (card.getDeckId() == TEST_DECK_ID)
                        && (card.getLastRepetitionDate() == TEST_CARD_LAST_REPETITION_DATE)
                        && (card.getFirstAddedTimestamp() == null));
    }

    @Test
    public void testMigrationFrom10To11ContainsCorrectData() throws IOException {
        ContentValues cardValues = new ContentValues();
        cardValues.put("card_id", TEST_CARD_ID);
        cardValues.put("question", TEST_CARD_QUESTION);
        cardValues.put("answer", TEST_CARD_ANSWER);
        cardValues.put("last_repetition_date", TEST_CARD_LAST_REPETITION_DATE);
        cardValues.put("deck_id", TEST_DECK_ID);
        TableInsertion cardTableInsertion = new TableInsertion(CARDS_TABLE_NAME, cardValues);

        ContentValues reviewLogValues = new ContentValues();
        reviewLogValues.put("review_log_id", TEST_REVIEW_LOG_ID);
        reviewLogValues.put("session_log_id", TEST_SESSION_LOG_ID);
        reviewLogValues.put("new_deck_id", TEST_NEW_DECK_ID);
        reviewLogValues.put("timestamp", TEST_TIMESTAMP);
        TableInsertion reviewTableInsertion = new TableInsertion(REVIEW_TABLE_NAME, reviewLogValues);

        CardsDatabase migratedDatabase = getMigratedDatabase(10, reviewTableInsertion, cardTableInsertion);

        migratedDatabase.reviewLogDao().selectReviewLogWithId(TEST_REVIEW_LOG_ID)
                .subscribeOn(mTestScheduler)
                .test()
                .assertValue(reviewLog -> reviewLog.getSessionLogId().equals(TEST_SESSION_LOG_ID)
                && reviewLog.getNewDeckId().equals(TEST_NEW_DECK_ID)
                && reviewLog.getTimestamp().equals(TEST_TIMESTAMP));

        migratedDatabase.cardDao().selectOneCard()
                .subscribeOn(mTestScheduler)
                .test()
                .assertValue(card -> card.getQuestion().equals(TEST_CARD_QUESTION)
                        && card.getAnswer().equals(TEST_CARD_ANSWER)
                        && (card.getDeckId() == TEST_DECK_ID)
                        && (card.getLastRepetitionDate() == TEST_CARD_LAST_REPETITION_DATE)
                        && (card.getFirstAddedTimestamp() == null));
    }

    class TableInsertion {
        private final String mTableName;
        private final ContentValues mContentValues;

        TableInsertion(String tableName, ContentValues contentValues) {
            mTableName = tableName;
            mContentValues = contentValues;
        }

        public String getTableName() {
            return mTableName;
        }

        public ContentValues getContentValues() {
            return mContentValues;
        }
    }
}
