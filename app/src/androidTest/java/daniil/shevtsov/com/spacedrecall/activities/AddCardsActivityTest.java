package daniil.shevtsov.com.spacedrecall.activities;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.test.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.R;
import daniil.shevtsov.com.spacedrecall.android.SessionTracker;
import daniil.shevtsov.com.spacedrecall.util.TestDatabaseUtil;
import daniil.shevtsov.com.spacedrecall.util.dagger.TestApplication;
import daniil.shevtsov.com.spacedrecall.view.ui.AddCardsActivity;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class AddCardsActivityTest {
    private static final long AWAIT_TIMEOUT = TestDatabaseUtil.getAwaitTimeout();

    private static final String QUESTION_WORD = "Dog";
    private static final String ANSWER_WORD = "Собака";

    private static final String TEST_DATABASE_NAME = "add_cards_test_database";

    @Inject
    TestDatabaseUtil mTestDatabaseUtil;

    @Inject
    SessionTracker mSessionTracker;

    private LifecycleRegistry mLifecycleRegistry;

    @Rule
    public final ActivityTestRule<AddCardsActivity> mActivityTestRule = new ActivityTestRule<>(
            AddCardsActivity.class, false, false);

    @Before
    public void onSetup() {
        TestApplication testApplication = (TestApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();

        testApplication.buildTestAppComponent();

        testApplication.getAppComponent().inject(this);

        mLifecycleRegistry = new LifecycleRegistry(ProcessLifecycleOwner.get());
        mLifecycleRegistry.addObserver(mSessionTracker);

        mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START);

        mTestDatabaseUtil.setDatabaseInMemory(TEST_DATABASE_NAME);

        mTestDatabaseUtil.populateTestDatabase();

        mActivityTestRule.launchActivity(null);
    }

    @After
    public void onTearDown() {
        mTestDatabaseUtil.tearDownTestDatabase();
    }

    @Test
    public void testInvalidQuestion() {
        onView(withId(R.id.add_card_question_edit_text)).perform(replaceText(""));

        onView(withId(R.id.add_card_button)).check(matches(not(isEnabled())));
    }

    @Test
    public void testInvalidAnswer() {
        onView(withId(R.id.add_card_question_edit_text)).perform(replaceText(""));

        onView(withId(R.id.add_card_button)).check(matches(not(isEnabled())));
    }

    @Test
    public void testValidQuestionAndAnswer() {
        onView(withId(R.id.add_card_question_edit_text)).perform(replaceText(QUESTION_WORD));
        onView(withId(R.id.add_card_answer_edit_text)).perform(replaceText(ANSWER_WORD));

        onView(withId(R.id.add_card_button)).check(matches(isEnabled()));
    }

    @Test
    public void testAddCardButton() {
        onView(withId(R.id.add_card_question_edit_text)).perform(replaceText(QUESTION_WORD), closeSoftKeyboard());
        onView(withId(R.id.add_card_answer_edit_text)).perform(replaceText(ANSWER_WORD), closeSoftKeyboard());

        onView(withId(R.id.add_card_button)).perform(click());

        onView(withText(R.string.card_inserted_message))
                .inRoot(withDecorView(not(is(mActivityTestRule.getActivity().getWindow().getDecorView()))))
                .check(matches(isDisplayed()));
    }

    @Test
    public void testInsertingCorrectAddedCardsCount() {
        mTestDatabaseUtil.getCardsDatabase().sessionLogDao().deleteAllSessionLogs();

        int addedCardsCount = 2;

        IntStream.range(0, addedCardsCount)
                .forEach(i ->addCard(QUESTION_WORD,ANSWER_WORD));

        mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP);

        mTestDatabaseUtil.getCardsDatabase().sessionLogDao().selectAllSessionLogs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(sessionLogs -> sessionLogs
                        .stream()
                        .anyMatch(sessionLog -> sessionLog.getAddedCardsCount().equals(addedCardsCount)));
    }

    private void addCard(String question, String answer) {
        onView(withId(R.id.add_card_question_edit_text)).perform(replaceText(question), closeSoftKeyboard());
        onView(withId(R.id.add_card_answer_edit_text)).perform(replaceText(answer), closeSoftKeyboard());

        onView(withId(R.id.add_card_button)).perform(click());
    }
}
